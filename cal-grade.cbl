       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CAL-GRADE.
       AUTHOR. KITTIKUN.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-GRADE-FILE VALUE HIGH-VALUE.
           06 COURSE-CODE PIC x(6).
           06 COURSE-NAME PIC x(50).
           06 CREDIT   PIC 9(1).
           06 GRADE    PIC 9(2).

       FD  AVG-FILE.
       01  AVG-DETAIL.
           06  AVG-GRADE PIC 9(1)v9(3).
           06  AVG-SCI-GRADE PIC 9(1)v9(3).
           06  AVG-CS-GRADE PIC 9(1)v9(3).

       WORKING-STORAGE SECTION. 
       01  SUM-CREDIT  PIC 9(3).
       01  SUM-CREDIT-SCI PIC 9(3).
       01  SUM-CREDIT-CS  PIC 9(3).
       01  GPA   PIC 9(3)v9(3).
       01  NUM-GRADE PIC 9(3)v9(3).
       01  GPA-SCI PIC 9(3)v9(3).
       01  GPA-CS  PIC 9(3)v9(3).
       

       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE 
           OPEN OUTPUT AVG-FILE 
           PERFORM UNTIL END-OF-GRADE-FILE
              READ GRADE-FILE 
                 AT END SET END-OF-GRADE-FILE TO TRUE
              END-READ
              IF NOT END-OF-GRADE-FILE THEN
                 PERFORM 001-PROCESS THRU 001-EXIT 
              END-IF 
           END-PERFORM
           DISPLAY  "ALL GRADE " AVG-GRADE IN AVG-DETAIL  " " GPA
                                            " "  SUM-CREDIT
           DISPLAY  "SCI " AVG-SCI-GRADE  " "GPA-SCI " " SUM-CREDIT-SCI
           DISPLAY  "CS " AVG-CS-GRADE  " "GPA-CS " " SUM-CREDIT-CS
           WRITE AVG-DETAIL 
           CLOSE GRADE-FILE 
           CLOSE AVG-FILE 
           GOBACK 
           .
       001-PROCESS.
           COMPUTE SUM-CREDIT = SUM-CREDIT + CREDIT
           EVALUATE TRUE 
              WHEN GRADE = "A" MOVE 4 TO NUM-GRADE 
              WHEN GRADE = "B+" MOVE 3.5 TO NUM-GRADE
              WHEN GRADE = "B" MOVE 3 TO NUM-GRADE
              WHEN GRADE = "C+" MOVE 2.5 TO NUM-GRADE 
              WHEN GRADE = "C" MOVE 2 TO NUM-GRADE
              WHEN GRADE = "D+" MOVE 1.5 TO NUM-GRADE
              WHEN GRADE = "D" MOVE 1 TO NUM-GRADE       
           END-EVALUATE
           COMPUTE GPA = GPA + (NUM-GRADE * CREDIT)
           COMPUTE AVG-GRADE IN AVG-DETAIL  = GPA / SUM-CREDIT 
           IF COURSE-CODE(1:1) = "3" THEN 
              COMPUTE GPA-SCI  = GPA-SCI  + (NUM-GRADE * CREDIT) 
              COMPUTE SUM-CREDIT-SCI  = SUM-CREDIT-SCI  + CREDIT
              COMPUTE AVG-SCI-GRADE IN AVG-DETAIL  = GPA-SCI  / 
                                                 SUM-CREDIT-SCI  
           END-IF 
           IF COURSE-CODE(1:2) = "31" THEN 
              COMPUTE GPA-CS  = GPA-CS  + (NUM-GRADE * CREDIT) 
              COMPUTE SUM-CREDIT-CS  = SUM-CREDIT-CS  + CREDIT
              COMPUTE AVG-CS-GRADE IN AVG-DETAIL  = GPA-CS  / 
                                                 SUM-CREDIT-CS  
           END-IF 
           
           .
       001-EXIT.
           EXIT.

           